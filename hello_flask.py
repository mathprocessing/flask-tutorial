from flask import Flask, render_template, request

app = Flask(__name__)

i = 0

@app.route('/')
def hello_world():
  global i
  i += 1
  return 'Hello, World!' + str(i)

if __name__ == '__main__':
  app.run()